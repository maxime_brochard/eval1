package test.java.com.eval1;

import main.java.com.eval1.MonetaryAmount;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MonetaryAmountTest {

    @Test
    void ShouldReturnMonetaryAmountWithDouble() {
        //Account aTest = new Account("Michel", "€");
        MonetaryAmount mATest = new MonetaryAmount(30.0, "€");

        Assertions.assertEquals(30.0, mATest.getAmount());
    }

    @Test
    void ShouldAdd() {
        //Account aTest = new Account("Michel", "€");
        MonetaryAmount mATest = new MonetaryAmount(10.0, "€");
        mATest.addAmount(30.0);

        Assertions.assertEquals(40.0, mATest.getAmount());
    }

    @Test
    void ShouldSubstract() {
        MonetaryAmount mATest = new MonetaryAmount(10.0, "€");
        mATest.substractAmount(4.0);

        Assertions.assertEquals(6.0, mATest.getAmount());
    }

    @Test
    void ShouldStringifyObject() {
        MonetaryAmount mATest = new MonetaryAmount(10.0, "€");
        Assertions.assertEquals("10.0€", mATest.toString());
    }


}
