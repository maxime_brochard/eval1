package main.java.com.eval1;


public class Courant extends Account {

    public double decouvertAuthorise = -1000.0;


    public Courant(String ownerName, String curency) {
        super(ownerName, curency);
    }

    @Override
    public void withdraw(double amount) {
        //get actual balance of account
        double actualBalance = balance.getAmount();
        //substract wanted amount of actual balance
        double hypotheticalResult = actualBalance - amount;

        //if the hypothetical result is above decouvertAuthorise , can withdraw
        if (hypotheticalResult > this.decouvertAuthorise) {
            this.getCurrentBalance().substractAmount(amount);
        }
    }
}
