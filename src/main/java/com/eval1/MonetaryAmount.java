package main.java.com.eval1;

public class MonetaryAmount {

    private double amount;
    private String currency;

    public MonetaryAmount(double amount, String currency) {
        this.amount = amount;
        this.currency = currency;
    }

    public double getAmount() {
        return this.amount;
    }

    public String getCurrency() {
        return this.currency;
    }

    public void addAmount(double amount) {
        this.amount = this.amount + amount;
    }

    public void substractAmount(double amount) {
        this.amount = this.amount - amount;
    }

    public String toString() {
        return amount + currency;
    }
}
