package main.java.com.eval1;

public class Main {
    public static void main(String[] args) {

        //2
        Courant a = new Courant("Obélix", "sesterces");

        //3
        Epargne b = new Epargne("Obélix", "$", 1, 0.01);

        //4
        a.deposit(100.0);
        b.deposit(30);

        //5
        a.withdraw(200);
        System.out.println(a.getCurrentBalance().getAmount());

        //6
        b.creditInterest();
        System.out.println(b.getCurrentBalance().getAmount());
    }
}
