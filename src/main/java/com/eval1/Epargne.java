package main.java.com.eval1;


public class Epargne extends Account {

    public MonetaryAmount decouvertAuthorise;
    public double interestFactor;


    public Epargne(String ownerName, String curency, double initialDeposit, double interestFactor) {
        super(ownerName, curency);//If initial deposit is above 0.0, deposit it
        if (initialDeposit <= 0.0 || interestFactor <= 0.0) {
            throw new IllegalArgumentException("Impossible de créer le compte sans un dépot supérieur à 0.0 ou un taux d'intérêt négatif");
        } else {
            this.deposit(initialDeposit);
            this.interestFactor = interestFactor;
        }

    }

    public void creditInterest() {
        double actualBalance = getCurrentBalance().getAmount();
        double factor = this.interestFactor;
        double interestAmount = actualBalance * factor;
        MonetaryAmount interestAmountObject = new MonetaryAmount(interestAmount, this.curency);

        this.deposit(interestAmountObject.getAmount());
    }

}