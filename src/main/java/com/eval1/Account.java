package main.java.com.eval1;

public class Account {

    final String curency;
    protected MonetaryAmount balance;
    private String ownerName;

    public Account(String ownerName, String curency) {
        this.ownerName = ownerName;
        this.curency = curency;

        this.balance = new MonetaryAmount(0.0, curency);

    }

    public void deposit(double amount) {
        this.balance.addAmount(amount);
    }

    public void withdraw(double amount) {
        double actualBalance = this.balance.getAmount();
        if (actualBalance <= amount) {
            return;
        } else {
            this.balance.substractAmount(amount);
        }
    }

    public MonetaryAmount getCurrentBalance() {
        return this.balance;
    }

    public String getOwnerName() {
        return this.ownerName;
    }
}