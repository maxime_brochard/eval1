package test.java.com.eval1;

import main.java.com.eval1.Account;
import main.java.com.eval1.Courant;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CourantTest {

    @Test
    void shouldAuthorizeDecouvert() {
        Courant aTest = new Courant("Michel", "€");
        aTest.withdraw(700);
        Assertions.assertEquals(-700, aTest.getCurrentBalance().getAmount());
    }

    @Test
    void Bug() {
        Courant aTest = new Courant("Michel", "€");
        aTest.deposit(10);
        aTest.withdraw(10);
        aTest.withdraw(10);
        aTest.withdraw(999);
        Assertions.assertEquals(-10, aTest.getCurrentBalance().getAmount());
    }
}
