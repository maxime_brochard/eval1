package test.java.com.eval1;

import main.java.com.eval1.Account;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class AccountTest {

    @Test
    void ShouldReturnInitialBalance() {
        Account aTest = new Account("Michel", "€");
        Assertions.assertEquals(0.0, aTest.getCurrentBalance().getAmount());
    }

    @Test
    void ShouldDeposit() {
        Account aTest = new Account("Michel", "€");
        aTest.deposit(30.0);
        aTest.deposit(30.0);

        Assertions.assertEquals(60.0, aTest.getCurrentBalance().getAmount());
    }

    @Test
    void ShouldWithdraw() {
        Account aTest = new Account("Michel", "€");
        aTest.deposit(50.0);
        aTest.withdraw(30.0);
        Assertions.assertEquals(20.0, aTest.getCurrentBalance().getAmount());
    }

    @Test
    void ShouldNotWithdraw() {
        Account aTest = new Account("Michel", "€");
        aTest.withdraw(30.0);
        Assertions.assertEquals(0.0, aTest.getCurrentBalance().getAmount());
    }

    @Test
    void shouldReturnOwnerName() {
        Account aTest = new Account("Michel", "€");
        Assertions.assertEquals("Michel", aTest.getOwnerName());
    }
}
