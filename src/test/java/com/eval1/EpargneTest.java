package test.java.com.eval1;

import main.java.com.eval1.Account;
import main.java.com.eval1.Epargne;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class EpargneTest {

    @Test
    void ShouldOpenWithInitialDeposit() {
        Epargne aTest = new Epargne("Michel", "€", 1.0, 0.03);
        Assertions.assertEquals(1, aTest.getCurrentBalance().getAmount());
    }

    @Test
    void ShouldDepositInterest() {
        Epargne aTest = new Epargne("Michel", "€", 100.0, 0.3);
        aTest.creditInterest();
        Assertions.assertEquals(130.0, aTest.getCurrentBalance().getAmount());
    }
}